﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

M.AutoInit();

$(document).ready(function () {
    //$('body').fadeIn('slow');
    $('#content').fadeIn('slow');

    $('.preloader-background').delay(200).fadeOut();

});

$(window).bind('beforeunload', function () {
    $('#content').fadeOut();
    $('.preloader-background').delay(0).fadeIn();
});