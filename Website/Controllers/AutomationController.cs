﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Website.Controllers
{
    public class AutomationController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Development()
        {
            return View();
        }

        public IActionResult Deployment()
        {
            return View();
        }

        public IActionResult Test()
        {
            return View();
        }

        public IActionResult Analytics()
        {
            return View();
        }
    }
}